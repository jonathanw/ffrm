# Future Forex RM

## System requirements

* Ubuntu 20.04 (20.04.2 LTS)
* Python 3 (3.8.10)
* Django 3.2.8

## Install Django

Install Django using Pip.

python3 -m pip install Django

## Running the Application

python3 manage.py runserver 0:8000

And then access on http://localhost:8000/

Or you can use the online dev version

- http://henny.plaas.co:8000/admin (user: jonathan, password: jonathan)
- http://henny.plaas.co:8000/rm/upload/2 (example document request that has already been uploaded)
- http://henny.plaas.co:8000/rm/upload/5/ (example of a document request that has not been uploaded yet)

*Note, I have left the DEBUG=True setting on*

## Database

This system is currently using the built in SQLite database that comes with Django.

## Usage

The relationship management application is a combination of using the built in Django Admin and some custom forms and templates.

![Screenshot](https://gitlab.com/jonathanw/ffrm/-/raw/master/resources/screenshots/Screenshot%202021-10-06%20at%2015.36.30.png "Relationship Management Models")

If you login to the Django Admin you will see the above Relationship Management block. This contains the 3 models:

* Relationship Managers
* Clients
* Client Documents

![Screenshot](https://gitlab.com/jonathanw/ffrm/-/raw/master/resources/screenshots/Screenshot%202021-10-06%20at%2015.36.40.png "Relationship Manager")

You can view, add, and edit the Relationship Managers.

![Screenshot](https://gitlab.com/jonathanw/ffrm/-/raw/master/resources/screenshots/Screenshot%202021-10-06%20at%2015.36.49.png "Clients")

You can view, add, and edit the Clients. Clients are all linked to a relationship manager via a foreign key. You can see which relationship manager the client is linked to in the listing table.

![Screenshot](https://gitlab.com/jonathanw/ffrm/-/raw/master/resources/screenshots/Screenshot%202021-10-06%20at%2015.36.57.png "Client Documents")

You can view, add, and edit the Client Documents.

### Adding A Client Document Request

![Screenshot](https://gitlab.com/jonathanw/ffrm/-/raw/master/resources/screenshots/Screenshot%202021-10-06%20at%2015.37.37.png "Adding a new client document request")

The Client Document model is used for both the request and the actual document upload. A relationship manager will create a new Client Document, but they will only fill in the Description and select the relevant client.

### Uploading The Document

![Screenshot](https://gitlab.com/jonathanw/ffrm/-/raw/master/resources/screenshots/Screenshot%202021-10-06%20at%2015.38.03.png "Uploading the document")

Once the request has been created then the relationship manager can send a unique link to the client. After that link is clicked on the required document can be uploaded and is then saved.

The link uses the Client Document ID. The link will look something like this: https://localhost:8000/rm/upload/1 where *1* will be replaced with the correct ID.

![Screenshot](https://gitlab.com/jonathanw/ffrm/-/raw/master/resources/screenshots/Screenshot%202021-10-06%20at%2015.38.18.png "Successfully uploaded")

The document can only be uploaded once by the client.

### Accessing The Document File

The uploaded document can be accessed through the Django Admin panel by going to Client Documents, choosing the right document, and then clicking on the link to the uploaded file. It will then download to the relationship managers computer.

![Screenshot](https://gitlab.com/jonathanw/ffrm/-/raw/master/resources/screenshots/Screenshot%202021-10-06%20at%2015.38.28.png "Accessing the document")

### Future Enhancements

Future enhancements can include the following:

* Auto emailing/SMSing the link to the client.
* Notifying the relationship manager via email or SMS when the client has uploaded the document
* Sending a reminder email if the client does not upload the document in time
* Adding extra security around the client document upload process, including using a better URL system that is less easy to guess.

## Other

Logo font is Paytone One from Google fonts: https://fonts.google.com/specimen/Paytone+One?query=paytone

The original logo file is an SVG file that can be found in the resources folder.

The custom templates are using Bootstrap 5.1.
