from django import forms
from .models import ClientDoc

class UploadClientDocForm(forms.ModelForm):
    class Meta:
        model = ClientDoc
        fields = ['d_file']
