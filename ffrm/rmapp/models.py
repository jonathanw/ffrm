from django.db import models
from django.urls import reverse

# Create your models here.

class RelMan(models.Model):
    rm_name = models.CharField(max_length=200, verbose_name='Name')

    def __str__(self):
        return self.rm_name

    class Meta:
        verbose_name = 'Relationship Manager'

class Client(models.Model):
    c_name = models.CharField(max_length=200, verbose_name='Name')
    c_phone = models.CharField(max_length=10, verbose_name='Phone')
    c_email = models.CharField(max_length=200, verbose_name='Email')
    c_rm = models.ForeignKey(RelMan, on_delete=models.CASCADE, verbose_name='Relationship Manager')

    def __str__(self):
        return self.c_name
    
    class Meta:
        verbose_name = 'Client'

class ClientDoc(models.Model):
    d_desc = models.CharField(max_length=200, verbose_name='Description')
    d_created = models.DateTimeField(verbose_name='Uploaded', null=True, blank=True)
    d_file = models.FileField(upload_to='uploads/%Y/%m/%d/', verbose_name='File', null=True, blank=True)
    d_client = models.ForeignKey(Client, on_delete=models.CASCADE, verbose_name='Client')

    def __str__(self):
        return self.d_desc
    
    class Meta:
        verbose_name = 'Client Document'

    def get_upload_link(self):
        return '%s' % (reverse('documentRequest', kwargs={'client_doc_id': self.id}))
