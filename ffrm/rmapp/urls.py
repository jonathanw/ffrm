from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('upload/<int:client_doc_id>/', views.documentRequest, name='documentRequest'),
]
