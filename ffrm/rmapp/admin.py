from django.contrib import admin
from .models import RelMan, Client, ClientDoc

class RelManAdmin(admin.ModelAdmin):
    list_display = ('rm_name',)

class ClientAdmin(admin.ModelAdmin):
    list_display = ('c_name', 'c_phone', 'c_email', 'c_rm')

class ClientDocAdmin(admin.ModelAdmin):
    list_display = ('d_desc', 'd_client', 'get_upload_link', 'd_created')

admin.site.register(RelMan, RelManAdmin)
admin.site.register(Client, ClientAdmin)
admin.site.register(ClientDoc, ClientDocAdmin)
