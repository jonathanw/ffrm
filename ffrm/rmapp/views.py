from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.utils import timezone
from rmapp.forms import UploadClientDocForm
from rmapp.models import ClientDoc

# Create your views here.

def index(request):
    return HttpResponse("Hello, world. You're at the Future Forex Relationship Manager index.")

def documentRequest(request, client_doc_id):
    client_doc = ClientDoc.objects.get(pk=client_doc_id)
    form = UploadClientDocForm()
    success = False;
    error_message = ''

    if client_doc.d_file:
        success = True;

    if request.method == 'POST':
        form = UploadClientDocForm(request.POST, request.FILES)

        if form.is_valid():
            if "d_file" not in request.FILES:
                error_message = 'You need to upload a file.'
            else: 
                client_doc.d_file = request.FILES['d_file']
                client_doc.d_created = timezone.now()
                client_doc.save()

                success = True        

    return render(request, 'upload.html', {'form': form, 'client_doc': client_doc, 'success': success, 'error_message': error_message})
